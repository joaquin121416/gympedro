package edu.cibertec.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.cibertec.dao.SchemeDAO;
import edu.cibertec.dto.SchemeMasterDTO;
import edu.cibertec.service.SchemeService;

@Service(value = "SchemeService")
public class SchemeServiceImpl implements SchemeService{

	@Autowired
	SchemeDAO schemeDAO;

	@Override
	public List<SchemeMasterDTO> obtenerSchemes() {
		// TODO Auto-generated method stub
		return schemeDAO.obtenerSchemes();
	}

	@Override
	public SchemeMasterDTO insertarScheme(SchemeMasterDTO scheme) {
		// TODO Auto-generated method stub
		return schemeDAO.insertarScheme(scheme);
	}


	
}
package edu.cibertec.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.cibertec.dao.RolDAO;
import edu.cibertec.dto.RolDTO;
import edu.cibertec.service.RolService;

@Service(value = "RolService")
public class RolServiceImpl implements RolService{

	@Autowired
	RolDAO rolDAO;

	@Override
	public List<RolDTO> obtenerRoles() {
		// TODO Auto-generated method stub
		return rolDAO.obtenerRoles();
	}

	@Override
	public RolDTO obtenerRolesById(int id) {
		return rolDAO.obtenerRolesById(id);
	}

	@Override
	public RolDTO insertarRol(RolDTO rol) {
		// TODO Auto-generated method stub
		return rolDAO.insertarRol(rol);
	}
	
}
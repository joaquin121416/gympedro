package edu.cibertec.service;

import edu.cibertec.dto.PeriodDTO;

public interface PeriodService {

	PeriodDTO insertarPeriod(PeriodDTO periodDTO);

	PeriodDTO actualizarPeriod(PeriodDTO periodDTO);
	
}

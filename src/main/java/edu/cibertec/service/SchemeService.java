package edu.cibertec.service;

import java.util.List;

import edu.cibertec.dto.SchemeMasterDTO;

public interface SchemeService {

	public List<SchemeMasterDTO> obtenerSchemes();

	public SchemeMasterDTO insertarScheme(SchemeMasterDTO scheme);
	
}

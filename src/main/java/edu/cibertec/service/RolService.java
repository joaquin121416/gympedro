package edu.cibertec.service;

import java.util.List;

import edu.cibertec.dto.RolDTO;

public interface RolService {

	List<RolDTO> obtenerRoles();

	RolDTO obtenerRolesById(int id);

	RolDTO insertarRol(RolDTO rol);
	
}

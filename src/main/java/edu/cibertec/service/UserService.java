package edu.cibertec.service;

import java.util.List;

import edu.cibertec.dto.LoginDTO;
import edu.cibertec.dto.UsuariosDTO;


public interface UserService {

	
	public List<UsuariosDTO> obtenerUsuarios();
	
	public LoginDTO validarLogin(LoginDTO l);

	public UsuariosDTO obtenerUsuarioById(int id);

}

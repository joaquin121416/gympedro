package edu.cibertec.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.cibertec.dto.PeriodDTO;
import edu.cibertec.dto.RolDTO;
import edu.cibertec.service.PeriodService;
import edu.cibertec.service.RolService;


@RestController
@RequestMapping("/api/v1")
public class PeriodController {

	private Logger logger = LoggerFactory.getLogger(PeriodController.class);
	
	@Autowired
	PeriodService periodService;
	
    @PutMapping(value = "/period", produces = MediaType.APPLICATION_JSON_VALUE)
    public PeriodDTO actualizarPeriod(@RequestBody PeriodDTO period) {
  
        return periodService.actualizarPeriod(period);
    }
    
    
    @PostMapping(value = "/period", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE )
    public PeriodDTO insertarPeriod(@RequestBody PeriodDTO period) {
    	
    	
    	logger.info("Info => " + period);
        return periodService.insertarPeriod(period);
    }
}

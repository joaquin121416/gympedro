package edu.cibertec.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import edu.cibertec.dto.RolDTO;
import edu.cibertec.dto.SchemeMasterDTO;
import edu.cibertec.service.RolService;
import edu.cibertec.service.SchemeService;


@RestController
@RequestMapping("/api/v1")
public class SchemeController {

	private Logger logger = LoggerFactory.getLogger(SchemeController.class);
	
	@Autowired
	SchemeService schemeService;
	
    @GetMapping(value = "/scheme", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SchemeMasterDTO> getSchemes() {
  
        return schemeService.obtenerSchemes();
    }
    
    
    @PostMapping(value = "/scheme", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE )
    public SchemeMasterDTO insertarScheme(@RequestBody SchemeMasterDTO scheme) {
    	
    	
    	logger.info("Info => " + scheme);
        return schemeService.insertarScheme(scheme);
    }
}

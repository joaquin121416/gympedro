package edu.cibertec.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.cibertec.bd.util.UtilBD;
import edu.cibertec.dao.RolDAO;
import edu.cibertec.dto.RolDTO;

@Repository(value = "RolDAO")
public class RolDAOImpl implements RolDAO {


	@Override
	public List<RolDTO> obtenerRoles() {
		List<RolDTO> lstRol = null;
		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cnx = UtilBD.getConnection();
			String sql = "select * from Role";
			pst = cnx.prepareStatement(sql);

			rs = pst.executeQuery();

			lstRol = new ArrayList<RolDTO>();

			while (rs.next()) {
				RolDTO rol = new RolDTO();

				rol.setRoleId(rs.getInt(1));
				rol.setRoleName(rs.getString(2));
				rol.setStatus(rs.getInt(3));
		

				lstRol.add(rol);
			}
		} catch (Exception e) {
			System.out.println("Error en llenacombo :" + e.getMessage());
		}

		return lstRol;
	}

	@Override
	public RolDTO obtenerRolesById(int id) {

		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		RolDTO rol = null;
		try {
			cnx = UtilBD.getConnection();
			String sql = "select * from Role where RoleId = ?";
			
			pst = cnx.prepareStatement(sql);
			pst.setInt(1, id);
			
			
			rs = pst.executeQuery();
			if(rs.next()) {
				rol =new RolDTO();
				
				rol.setRoleId(rs.getInt(1));
				rol.setRoleName(rs.getString(2));
				rol.setStatus(rs.getInt(3));
			}
			
			
		} catch (Exception e) {
			System.out.println("Error en la sentencia" + e.getMessage());
		} 

		return rol;
	}
	
	@Override
	public RolDTO insertarRol(RolDTO rol) {

		Connection cnx = null;
		PreparedStatement pst = null;
		try {
			cnx = UtilBD.getConnection();
			String sql = "insert into  Role (RoleName,Status) values(?,?);";
			
			pst = cnx.prepareStatement(sql);
			pst.setString(1, rol.getRoleName());
			pst.setInt(2, rol.getStatus());
			
			
			pst.execute();
		
			
			
		} catch (Exception e) {
			System.out.println("Error en la sentencia" + e.getMessage());
		} 

		return rol;
	}

}

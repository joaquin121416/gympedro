package edu.cibertec.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.cibertec.bd.util.UtilBD;
import edu.cibertec.dao.SchemeDAO;
import edu.cibertec.dto.SchemeMasterDTO;

@Repository(value = "SchemeDAO")
public class SchemeDAOImpl implements SchemeDAO {


	@Override
	public List<SchemeMasterDTO> obtenerSchemes() {
		List<SchemeMasterDTO> lstScheme = null;
		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cnx = UtilBD.getConnection();
			String sql = "Select * from SchemeMaster";
			pst = cnx.prepareStatement(sql);

			rs = pst.executeQuery();

			lstScheme = new ArrayList<SchemeMasterDTO>();

			while (rs.next()) {
				SchemeMasterDTO rol = new SchemeMasterDTO();

				rol.setSchemeID(rs.getInt(1));
				rol.setSchemeName(rs.getString(2));
				rol.setCreatedby(rs.getInt(3));
				rol.setCreateddate(rs.getDate(4));
				rol.setStatus(rs.getInt(5));
		

				lstScheme.add(rol);
			}
		} catch (Exception e) {
			System.out.println("Error en llenacombo :" + e.getMessage());
		}

		return lstScheme;
	}
	
	@Override
	public SchemeMasterDTO insertarScheme(SchemeMasterDTO scheme) {

		Connection cnx = null;
		PreparedStatement pst = null;
		try {
			cnx = UtilBD.getConnection();
			String sql = "insert into  SchemeMaster (SchemeName,Createdby,Createddate,Status) values(?,?,getdate(),?);;";
			
			pst = cnx.prepareStatement(sql);
			pst.setString(1, scheme.getSchemeName());
			pst.setInt(2, scheme.getCreatedby());
			pst.setInt(3, scheme.getStatus());
			
			pst.execute();
		
			
			
		} catch (Exception e) {
			System.out.println("Error en la sentencia" + e.getMessage());
		} 

		return scheme;
	}

}

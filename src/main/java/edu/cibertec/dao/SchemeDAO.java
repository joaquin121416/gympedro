package edu.cibertec.dao;

import java.util.List;

import edu.cibertec.dto.SchemeMasterDTO;


public interface SchemeDAO {

	public List<SchemeMasterDTO> obtenerSchemes();

	public SchemeMasterDTO insertarScheme(SchemeMasterDTO scheme);

}

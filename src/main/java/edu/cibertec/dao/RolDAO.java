package edu.cibertec.dao;

import java.util.List;

import edu.cibertec.dto.RolDTO;


public interface RolDAO {
	
	public List<RolDTO> obtenerRoles();
	
	public RolDTO obtenerRolesById(int id);

	RolDTO insertarRol(RolDTO rol);
	
}

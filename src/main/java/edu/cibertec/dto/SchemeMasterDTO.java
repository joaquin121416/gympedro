package edu.cibertec.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SchemeMasterDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1551399009628129323L;
	
	
	private int schemeID;
	private String schemeName;
	private int createdby;
	private Date createddate;
	private int status;
	
	public SchemeMasterDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SchemeMasterDTO(int schemeID, String schemeName, int createdby, Date createddate, int status) {
		super();
		this.schemeID = schemeID;
		this.schemeName = schemeName;
		this.createdby = createdby;
		this.createddate = createddate;
		this.status = status;
	}
	
	public int getSchemeID() {
		return schemeID;
	}
	public void setSchemeID(int schemeID) {
		this.schemeID = schemeID;
	}
	public String getSchemeName() {
		return schemeName;
	}
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	public int getCreatedby() {
		return createdby;
	}
	public void setCreatedby(int createdby) {
		this.createdby = createdby;
	}
	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SchemeMasterDTO [schemeID=" + schemeID + ", schemeName=" + schemeName + ", createdby=" + createdby
				+ ", createddate=" + createddate + ", status=" + status + "]";
	}



}

package edu.cibertec.dto;

import java.io.Serializable;
import java.sql.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuariosDTO implements Serializable {

	private int UserId;
	private String UserName;
	private String FullName;
	private String EmailId;
	private String Contactno;
	private String Password;
	private int Createdby;
	private Date CreatedDate;
	private int Status;

	public UsuariosDTO(String userName, String fullName, String emailId, String contactno, String password,
			int createdby, Date createdDate, int status) {
		super();
		UserName = userName;
		FullName = fullName;
		EmailId = emailId;
		Contactno = contactno;
		Password = password;
		Createdby = createdby;
		CreatedDate = createdDate;
		Status = status;
	}

	public UsuariosDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getUserId() {
		return UserId;
	}

	public void setUserId(int userId) {
		UserId = userId;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getContactno() {
		return Contactno;
	}

	public void setContactno(String contactno) {
		Contactno = contactno;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public int getCreatedby() {
		return Createdby;
	}

	public void setCreatedby(int createdby) {
		Createdby = createdby;
	}

	public Date getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}
	
	

}

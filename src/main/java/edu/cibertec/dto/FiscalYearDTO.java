package edu.cibertec.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiscalYearDTO implements Serializable {

	private int Fid;
	private Date fiscalYearFromDate;
	private Date fiscalYearToDate;
	private String Year;

	public FiscalYearDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FiscalYearDTO(int fid, Date fiscalyearFromDate, Date fiscalyearToDate, String year) {
		super();
		Fid = fid;
		this.fiscalYearFromDate = fiscalyearFromDate;
		this.fiscalYearToDate = fiscalyearToDate;
		Year = year;
	}

}

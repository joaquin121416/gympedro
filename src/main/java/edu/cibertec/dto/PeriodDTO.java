package edu.cibertec.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeriodDTO implements Serializable {

	private int periodID;
	private String text;
	private String value;
	public int getPeriodID() {
		return periodID;
	}
	public void setPeriodID(int periodID) {
		this.periodID = periodID;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public PeriodDTO(int periodID, String text, String value) {
		super();
		this.periodID = periodID;
		this.text = text;
		this.value = value;
	}
	public PeriodDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "PeriodDTO [periodID=" + periodID + ", text=" + text + ", value=" + value + "]";
	}


}

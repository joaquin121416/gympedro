package edu.cibertec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsersInRolesDTO {

	private int UserRolesId;
	private int UserId;
	private int RoleId;

	public UsersInRolesDTO(int userRolesId, int userId, int roleId) {
		super();
		UserRolesId = userRolesId;
		UserId = userId;
		RoleId = roleId;
	}

	public UsersInRolesDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

}

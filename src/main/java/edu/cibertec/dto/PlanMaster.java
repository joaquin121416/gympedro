package edu.cibertec.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlanMaster implements Serializable {

	private int PlanID;
	private String PlanName;
	private double PlanAmount;
	private double ServicetaxAmount;
	private String ServiceTax;
	private Date CreateDate;
	private int CreateUserID;
	private Date ModifyDate;
	private int ModifyUserID;
	private int RecStatus;
	private int SchemeID;
	private int PeriodID;
	private double TotalAmount;
	private String ServicetaxNo;

	public PlanMaster(int planID, String planName, double planAmount, double servicetaxAmount, String serviceTax,
			Date createDate, int createUserID, Date modifyDate, int modifyUserID, int recStatus, int schemeID,
			int periodID, double totalAmount, String servicetaxNo) {
		super();
		PlanID = planID;
		PlanName = planName;
		PlanAmount = planAmount;
		ServicetaxAmount = servicetaxAmount;
		ServiceTax = serviceTax;
		CreateDate = createDate;
		CreateUserID = createUserID;
		ModifyDate = modifyDate;
		ModifyUserID = modifyUserID;
		RecStatus = recStatus;
		SchemeID = schemeID;
		PeriodID = periodID;
		TotalAmount = totalAmount;
		ServicetaxNo = servicetaxNo;
	}

	public PlanMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

}

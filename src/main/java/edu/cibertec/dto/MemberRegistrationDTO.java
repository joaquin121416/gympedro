package edu.cibertec.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberRegistrationDTO implements Serializable {

	private Long MemberId;
	private String MemberNo;
	private String MemberFName;
	private String MemberLName;
	private String MemberMName;
	private Date DOB;
	private String Age;
	private String Contactno;
	private String EmailID;
	private String Gender;
	private int PlanID;
	private int SchemeID;
	private Long Createdby;
	private Date CreatedDate;
	private Date ModifiedDate;
	private Long ModifiedBy;
	private String MemImagename;
	private String MemImagePath;
	private Date JoiningDate;
	private String Address;
	private Long MainMemberID;

	public MemberRegistrationDTO(Long memberId, String memberNo, String memberFName, String memberLName,
			String memberMName, Date dOB, String age, String contactno, String emailID, String gender, int planID,
			int schemeID, Long createdby, Date createdDate, Date modifiedDate, Long modifiedBy, String memImagename,
			String memImagePath, Date joiningDate, String address, Long mainMemberID) {
		super();
		MemberId = memberId;
		MemberNo = memberNo;
		MemberFName = memberFName;
		MemberLName = memberLName;
		MemberMName = memberMName;
		DOB = dOB;
		Age = age;
		Contactno = contactno;
		EmailID = emailID;
		Gender = gender;
		PlanID = planID;
		SchemeID = schemeID;
		Createdby = createdby;
		CreatedDate = createdDate;
		ModifiedDate = modifiedDate;
		ModifiedBy = modifiedBy;
		MemImagename = memImagename;
		MemImagePath = memImagePath;
		JoiningDate = joiningDate;
		Address = address;
		MainMemberID = mainMemberID;
	}

	public MemberRegistrationDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

}

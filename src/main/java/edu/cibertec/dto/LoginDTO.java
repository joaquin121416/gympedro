package edu.cibertec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDTO {

	private String userName;
	private String password;
	private int roleId;

	public LoginDTO(String userName, String password, int roleId) {
		super();
		this.userName = userName;
		this.password = password;
		this.roleId = roleId;
	}

	public LoginDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "LoginDTO [userName=" + userName + ", password=" + password + ", roleId=" + roleId + "]";
	}
	
	
	

}

package edu.cibertec.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RolDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8052594134520495352L;
	
	private int roleId;
	private String roleName;
	private int status;

	public RolDTO(int roleId, String roleName, int status) {
		super();
		this.roleId = roleId;
		this.roleName = roleName;
		this.status = status;
	}

	public RolDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "RolDTO [RoleId=" + roleId + ", RoleName=" + roleName + ", Status=" + status + "]";
	}

	
	
}	

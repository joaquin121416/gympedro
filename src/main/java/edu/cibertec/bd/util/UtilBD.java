package edu.cibertec.bd.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class UtilBD {

	private static Connection connection;

	private UtilBD() {

	}

	public static Connection getConnection() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
 
			connection = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;database=AngularGYMDB", "dontito", "sql");
			System.out.println("Conectado.");
		} catch (SQLException ex) {
			System.out.println("Error." + ex.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Connection prueba = UtilBD.getConnection();
		if (prueba != null) {
			System.out.println("conectado");
			System.out.println(prueba);
			Statement stm = prueba.createStatement();
			ResultSet rst = stm.executeQuery("select * from Users");

			while (rst.next()) {
				System.out.println(rst.getString(2));

			}
		} else {
			System.out.println("Desconectado");
		}
	}

}
